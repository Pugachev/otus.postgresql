﻿using System.Linq;

namespace OTUS.PostgreSQL.Data
{
	public interface IDataRepository<T> 
		where T: class
	{
		void Create(T entity);
		IQueryable<T> GetAll();
	}
}
