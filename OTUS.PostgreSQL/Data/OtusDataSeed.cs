﻿using Microsoft.EntityFrameworkCore;
using OTUS.PostgreSQL.Models;
using System;

namespace OTUS.PostgreSQL.Data
{
	static class OtusDataSeed
	{
		public static void Create(this ModelBuilder builder)
		{
			builder.Entity<Course>().HasData
			(
				new Course { id = 1, name = "Java Developer Professional", startdate = DateTime.Parse("29-03-2021 11:00:00"), cost = 44000 },
				new Course { id = 2, name = "Web-разработчик на Python", startdate = DateTime.Parse("29-03-2021 11:00:00"), cost = 33000 },
				new Course { id = 3, name = "Unity Game Developer", startdate = DateTime.Parse("29-03-2021 11:00:00"), cost = 50000 },
				new Course { id = 4, name = "Алгоритмы и структуры данных", startdate = DateTime.Parse("29-03-2021 11:00:00"), cost = 37000},
				new Course { id = 5, name = "Kotlin Backend Developer", startdate = DateTime.Parse("29-03-2021 11:00:00"), cost= 500000}
			);

			builder.Entity<Student>().HasData
			(
				new Student { id = 1, firstname= "Михаил", lastname= "Горшков", age = 33, courseid = 4},
				new Student { id = 2, firstname= "Сергей", lastname= "Окатов", age = 51, courseid = 5},
				new Student { id = 3, firstname= "Михаил", lastname= "Иванов", age = 32, courseid = 2},
				new Student { id = 4, firstname= "Сергей", lastname= "Петрелевич", age = 40, courseid = 1},
				new Student { id = 5, firstname= "Николай", lastname= "Запольнов", age = 26, courseid = 3}
			);

			builder.Entity<Teacher>().HasData
			(
				new Teacher { id = 1, firstname = "Иван", lastname = "Иванов", age = 11, courseid = 4 },
				new Teacher { id = 2, firstname = "Расул", lastname = "Расулов", age = 35, courseid = 5 },
				new Teacher { id = 3, firstname = "Казбек", lastname = "Магомедхаджиев", age = 29, courseid = 1 },
				new Teacher { id = 4, firstname = "Петр", lastname = "Чечерин", age = 20, courseid = 2 },
				new Teacher { id = 5, firstname = "Купер", lastname = "Бредли", age = 47, courseid = 3 }
			);
		}
	}
}
