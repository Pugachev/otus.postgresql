﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OTUS.PostgreSQL.Models;

namespace OTUS.PostgreSQL.Data
{
	public class OtusDbContext : DbContext
	{
		public DbSet<Course> courses { get; set; }
		public DbSet<Student> students { get; set; }
		public DbSet<Teacher> teachers { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (optionsBuilder.IsConfigured)
				return;

			var configurationBuilder = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

			IConfigurationRoot configuration = configurationBuilder.Build();

			optionsBuilder.UseNpgsql(configuration.GetConnectionString("PostgreSQL"));
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.Create();
		}
	}
}
