﻿using System.Linq;

namespace OTUS.PostgreSQL.Data
{
	public class NpgSqlDataRepository<T> : IDataRepository<T>
		where T : class
	{
		private OtusDbContext _dataContext;

		public NpgSqlDataRepository(OtusDbContext dataContext)
		{
			_dataContext = dataContext;
		}

		public void Create(T entity)
		{
			_dataContext.Add(entity);
			_dataContext.SaveChanges();
		}

		public IQueryable<T> GetAll()
		{
			return _dataContext.Set<T>();
		}
	}
}
