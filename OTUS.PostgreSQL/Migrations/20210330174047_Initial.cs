﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace OTUS.PostgreSQL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "courses",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    startdate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    cost = table.Column<decimal>(type: "money", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_courses", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "students",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    firstname = table.Column<string>(type: "text", nullable: true),
                    lastname = table.Column<string>(type: "text", nullable: true),
                    age = table.Column<int>(type: "integer", nullable: false),
                    courseid = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_students", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "teachers",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    firstname = table.Column<string>(type: "text", nullable: true),
                    lastname = table.Column<string>(type: "text", nullable: true),
                    age = table.Column<int>(type: "integer", nullable: false),
                    courseid = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_teachers", x => x.id);
                });

            migrationBuilder.InsertData(
                table: "courses",
                columns: new[] { "id", "cost", "name", "startdate" },
                values: new object[,]
                {
                    { 1, 44000m, "Java Developer Professional", new DateTime(2021, 3, 29, 11, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 33000m, "Web-разработчик на Python", new DateTime(2021, 3, 29, 11, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 50000m, "Unity Game Developer", new DateTime(2021, 3, 29, 11, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 4, 37000m, "Алгоритмы и структуры данных", new DateTime(2021, 3, 29, 11, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5, 500000m, "Kotlin Backend Developer", new DateTime(2021, 3, 29, 11, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "students",
                columns: new[] { "id", "age", "courseid", "firstname", "lastname" },
                values: new object[,]
                {
                    { 1, 33, 4, "Михаил", "Горшков" },
                    { 2, 51, 5, "Сергей", "Окатов" },
                    { 3, 32, 2, "Михаил", "Иванов" },
                    { 4, 40, 1, "Сергей", "Петрелевич" },
                    { 5, 26, 3, "Николай", "Запольнов" }
                });

            migrationBuilder.InsertData(
                table: "teachers",
                columns: new[] { "id", "age", "courseid", "firstname", "lastname" },
                values: new object[,]
                {
                    { 1, 11, 4, "Иван", "Иванов" },
                    { 2, 35, 5, "Расул", "Расулов" },
                    { 3, 29, 1, "Казбек", "Магомедхаджиев" },
                    { 4, 20, 2, "Петр", "Чечерин" },
                    { 5, 47, 3, "Купер", "Бредли" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "courses");

            migrationBuilder.DropTable(
                name: "students");

            migrationBuilder.DropTable(
                name: "teachers");
        }
    }
}
