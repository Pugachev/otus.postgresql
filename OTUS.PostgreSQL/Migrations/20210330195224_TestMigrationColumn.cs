﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OTUS.PostgreSQL.Migrations
{
    public partial class TestMigrationColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "TestMigrationColumn",
                table: "students",
                type: "numeric",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TestMigrationColumn",
                table: "students");
        }
    }
}
