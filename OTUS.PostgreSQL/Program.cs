﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OTUS.PostgreSQL.Data;
using OTUS.PostgreSQL.Provider;
using OTUS.PostgreSQL.Services;
using System;
using System.Text;
using System.Threading.Tasks;

namespace OTUS.PostgreSQL
{
	class Program
	{
		public static void Main(string[] args)
		{
			MainAsync().GetAwaiter().GetResult();
		}
		
		private static async Task MainAsync()
		{
			Console.OutputEncoding = Encoding.UTF8;

			var host = Host.CreateDefaultBuilder()
				.ConfigureServices((context, services) =>
				{
					services.AddTransient(typeof(IDataRepository<>), typeof(NpgSqlDataRepository<>));
					services.AddTransient(typeof(IOutputProvider), typeof(ConsoleProvider));
					services.AddDbContext<OtusDbContext>(ServiceLifetime.Transient);
				}).Build();

			var dataContext = ActivatorUtilities.CreateInstance<OtusDbContext>(host.Services);

			var otusService = ActivatorUtilities.CreateInstance<OtusService>(host.Services);

			await otusService.GetAllTables();

			otusService.AddStudent("Kirill", "Petrov" , 20);

			await otusService.GetAllTables();

			Console.ReadLine();

		}
	}
}
