﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OTUS.PostgreSQL.Models
{
	public class Student
	{
		[Key]
		public int id { get; set; }

		public string firstname { get; set; }

		public string lastname { get; set; }

		public int age { get; set; }

		[Required]
		[ForeignKey("courses")]
		public int courseid { get; set; }

		public decimal TestMigrationColumn { get; set; }
	}
}
