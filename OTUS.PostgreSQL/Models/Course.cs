﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OTUS.PostgreSQL.Models
{
	public class Course
	{
		[Key]
		public int id { get; set; }

		public string name { get; set; }

		public DateTime startdate { get; set; }

		[Column(TypeName = "money")]
		public decimal cost { get; set; }
	}
}
