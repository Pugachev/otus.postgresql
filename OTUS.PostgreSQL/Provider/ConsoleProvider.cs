﻿using System;
using System.Text.Json;

namespace OTUS.PostgreSQL.Provider
{
	public class ConsoleProvider : IOutputProvider
	{
		public void Output<T>(T input)
		{
			string inputSerialize = JsonSerializer.Serialize<T>(input, new JsonSerializerOptions() { WriteIndented = true });
			Console.WriteLine(inputSerialize);
		}
	}
}
