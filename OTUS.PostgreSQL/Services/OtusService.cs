﻿using Microsoft.EntityFrameworkCore;
using OTUS.PostgreSQL.Data;
using OTUS.PostgreSQL.Models;
using OTUS.PostgreSQL.Provider;
using System;
using System.Threading.Tasks;

namespace OTUS.PostgreSQL.Services
{
	class OtusService : IOtusService
	{
		private readonly IDataRepository<Course> _courseRepository;
		private readonly IDataRepository<Student> _studentRepository;
		private readonly IDataRepository<Teacher> _teacherRepository;
		private readonly IOutputProvider _outputProvider;

		public OtusService(
			IDataRepository<Course> courseRepository,
			IDataRepository<Student> studentRepository,
			IDataRepository<Teacher> teacherRepository,
			IOutputProvider outputProvider)
		{
			_courseRepository = courseRepository;
			_studentRepository = studentRepository;
			_teacherRepository = teacherRepository;
			_outputProvider = outputProvider;
		}

		public async Task GetAllTables()
		{
			var courses = await _courseRepository.GetAll().ToListAsync();
			_outputProvider.Output(courses);

			var students = await _studentRepository.GetAll().ToListAsync();
			_outputProvider.Output(students);

			var teachers = await _teacherRepository.GetAll().ToListAsync();
			_outputProvider.Output(teachers);

			return;
		}

		public void AddStudent(
			string firstName,
			string lastName,
			int age)
		{
			var tudent = new Student()
			{
				id = 13,
				firstname = firstName,
				lastname = lastName,
				age = age,
				courseid = 1
			};

			_studentRepository.Create(tudent);
		}
	}
}
