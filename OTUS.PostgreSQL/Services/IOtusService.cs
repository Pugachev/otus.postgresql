﻿using System.Threading.Tasks;

namespace OTUS.PostgreSQL.Services
{
	public interface IOtusService
	{
		Task GetAllTables();

		void AddStudent(
			string firstName,
			string lastName,
			int age);
	}
}
