SET timezone=UTC;

INSERT INTO public.courses(id, name, startdate, cost)
VALUES 
	(1, 'Java Developer Professional', '29-03-2021 11:00:00', 44000),
	(2, 'Web-����������� �� Python', '30-03-2021 11:00:00', 33000),
	(3, 'Unity Game Developer', '15-04-2021 11:00:00', 50000),
	(4, '��������� � ��������� ������', '29-03-2021 11:00:00', 37000),
	(5, 'Kotlin Backend Developer', '22-04-2021 11:00:00', 500000);
	
INSERT INTO public.students(id, firstname, lastname, age, courseid)
VALUES 
	(1, '������', '�������', 33, 4),
	(2, '������', '������', 51, 5),
	(3, '������', '������', 32, 2),
	(4, '������', '����������', 40, 1),
	(5, '�������', '���������', 26, 3);
	
INSERT INTO public.teachers(
	id, firstname, lastname, age, courseid)
	VALUES 
	(1, '����', '������', 11, 4),
	(2, '�����', '�������', 35, 5),
	(3, '������', '��������������', 29, 1),
	(4, '����', '�������', 20, 2),
	(5, '�����', '������', 47, 3);