Create Table Teachers
(Id serial Primary key,
FirstName CHARACTER VARYING(30),
LastName CHARACTER VARYING(30),
Age INTEGER,
CourseId Integer references Course (Id));

Create Table Courses
(
	Id SERIAL PRIMARY KEY,
    Name VARCHAR(20) NOT NULL, 
    StartDate timestamp not null
);

Create Table Students
(
	Id serial Primary key,
	FirstName CHARACTER VARYING(30),
	LastName CHARACTER VARYING(30),
	Age INTEGER,
	CourseId Integer,
    FOREIGN KEY (CourseId) REFERENCES Courses (Id)
);

Alter table Courses 
add Cost Money